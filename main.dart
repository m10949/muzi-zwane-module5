import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Spaza App';

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text (_title)),
        body: Center(
          child: ElevatedButton(
            child: const Text('Next'),
            onPressed: () {
              Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const SignInpage()),
          },
        )),
    ),);
  }
}
