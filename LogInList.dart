import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class _logInLists extends StatefulWidget {
  const _logInLists({Key? key}) : super(key: key);

  @override
  State<_logInLists> createState() => _logInListsState();
}

class _logInListsState extends State<_logInLists> {

  TextEditingController  _nameController = TextEditingController();
  TextEditingController _surnameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();



 final Stream<QuerySnapshot> _mySignIn = FirebaseFirestore.instance.collection("logIns").snapshots();

  @override
  Widget build(BuildContext context) {
    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("logIns")
          .doc(docId)
          .delete()
          .then((value) => print("deleted"));
    }

    void _update(data) {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text('Update'),
          content: Column(
            mainAxisSize = mainAxisSize.min,
            children: [
              TextField(
                controller: _nameController

              ),
              TextField (
                controller: _surnameController
            
            ),   
              TextField(
                controller: _passwordController
                ),
              TextButton (
              onPressed () {}, child: const Text ("Update Button"),
            )

            ]
          );
        ) 
      );
      
    }
    return StreamBuilder(
      stream: _mySignIn,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return const Text ("Error");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }

        if (snapshot.hasData ) {
          return Row (
            children: [
              Expanded(
                child: SizedBox(
                  height: (MediaQuery.of(context).size.height),
                  width: (MediaQuery.of(context).size.width),

                  child: ListView(
                    children: snapshot.data!.docs
                        .map((DocumentSnapshot documentSnapshot) {
                          Map<String, dynamic> data =
                              documentSnapshot.data()! as Map<String, dynamic >;


                          return Column(
                            children: [
                              Card(
                                child: Column(
                                  children: [
                                    ListTile(
                                      title: Text(data['User_Name']),
                                      subtitle: Text(data['surname']),
                                      //subtitle: Text(data['password']),
                                      ),
                                    ButtonTheme(
                                        child: ButtonBar(
                                          children: [
                                            OutlineButton.icon(
                                                onPressed: () {},
                                                icon: Icon(Icons.edit),
                                                label: const Text("Edit"),),
                                            OutlineButton.icon(
                                              onPressed: () {},
                                              icon: Icon(Icons.remove),
                                              label: const Text("Delete"),),
                                          ],
                                        ) )
                                  ],
                                ),
                              )
                            ],
                          );


                    }).toList(),

                  ),
                ),
              )
            ],
          );
        } else {
          return const Text ("no data");
        }

      },);
  }
}
